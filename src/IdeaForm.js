import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import EditableLabel from 'react-inline-editing';
import Collapsible from 'react-collapsible';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

import HelperTooltip from './components/HelperTooltip';

const addSpaceToCamelCase = (str) => str.replace(/([a-zA-Z])(?=[A-Z])/g, '$1 ').toLowerCase();

class IdeaForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentStep: 1,
            substeps: [], // step 4 substeps
            currentSubstep: '', // current step 4 substep

            ideaName: '',
            category:  '',
            percentChange: '',
            upfrontCost: '',
            annualCost: '',

            expenseChecked: false,
            expenseName: "",
            expenseValue: "",
            expenseChange: "",
            expenseUnit: "%",
            expenseValues: [],

            subfields:{
                recruitingCosts: [false, '', '%'],
                turnoverRate: [false, '', '%'],
                rent: [false, '', '%'],
                price: [false, '', '%'],
                volume: [false, '', '%'],
                debt: [false, '', '%'],
                interestRate: [false, '', '%'],
                margin: [false, '', '%'],
                directCosts: [false, '', '%'],
                indirectCosts: [false, '', '%'],
                carryingCosts: [false, '', '%'],
                productionCapacity: [false, '', '%'],
                qualityControl: [false, '', '%'],
            },

            unit:{
                recruitingCosts: '$',
                turnoverRate: '%',
                rent: '$',
                price: '$',
                volume: 'Units',
                debt: '$',
                interestRate: '%',
                margin: '%',
                directCosts: '$',
                indirectCosts: '$',
                carryingCosts: '$',
                productionCapacity: '$',
                qualityControl: '$',
            },

            assumptions:{
                recruitingCosts: [],
                turnoverRate: ['headcount', 'onboardingCost'],
                rent: [],
                price: ['margin'],
                volume: ['margin'],
                debt: ['interestRate'],
                interestRate: ['debt'],
                margin: ['price', 'volume'],
                directCosts: [],
                indirectCosts: [],
                carryingCosts: [],
                productionCapacity: [],
                qualityControl: [],
            },
        }
    }

    updateSubfieldValue = (key, value) => {
      const arr = [...this.state.subfields[key]];
      arr[1] = value;
      this.setState({
          subfields: {
              ...this.state.subfields,
              [key]: arr,
          }
      });
    };

    setSubsteps = (arr) => {
        this.setState({
            substeps: arr,
        });
    };

    setCurrentSubstep = (str) => {
        this.setState({
           currentSubstep: str,
        });
    };

    handleChange = event => {
        const {name, value} = event.target;
        this.setState({
            [name]: value
        })
    };

    handleLoad = (name, value) => {
        this.setState({
            [name]: value
        })
    };

    handleSubmit = event => {
        event.preventDefault();
        if (event.target.keyCode === 13) {
            return;
        }
        var alertMessage = "";
        if (isNaN(this.state.upfrontCost) && isNaN(this.state.annualCost)) {
            alertMessage += "Upfront and Annual Costs not valid numbers.\n";
        } else {
            if (isNaN(this.state.upfrontCost)) {
                alertMessage += "Upfront Costs not a valid number.\n";
            } else if (this.state.upfrontCost < 0) {
                alertMessage += "Upfront Cost cannot be negative.\n"
            }

            if (isNaN(this.state.annualCost)) {
                alertMessage +="Annual Costs not a valid number.\n";
            } else if (this.state.annualCost < 0) {
                alertMessage += "Annual Costs cannot be negative.\n"
            }
        }

        if (alertMessage !== ""){
            alert(alertMessage);
        }else {
            const {ideaName, subfields, expenseValues, upfrontCost, annualCost} = this.state;
            this.props.submitCallback(ideaName, subfields, expenseValues,upfrontCost, annualCost);
        }
    };

    _next = () => {
        let { currentStep } = this.state;
        const { currentSubstep, substeps } = this.state;

        if (currentStep === 2 && !this.state.expenseChecked) {
            // step 3 is optional expense step, skip to step 4 if not "expenses" not checked
            currentStep = 4;
        } else if (currentStep === 4) {
            // step 4 has multiple substeps (with dynamic length)
            // clicking next on step 4 will progress currentSubstep and not progress currentStep
            // unless we are on last available substep, in which case we will progress currentStep
            const isLastSubstep = currentSubstep === substeps[substeps.length - 1];
            if (!isLastSubstep) {
                const currentIndex = substeps.indexOf(currentSubstep);
                const nextSubstepIndex = currentIndex + 1;
                const nextSubstep = substeps[nextSubstepIndex];

                this.setState({
                    currentSubstep: nextSubstep,
                });
            } else {
                currentStep += 1;
            }
        } else {
            currentStep += 1;
        }

        if (currentStep === 4 && substeps.length === 0){
            currentStep = 5;
        }

        this.setState({ currentStep });
    };

    _prev = () => {
        let { currentStep } = this.state;
        const { currentSubstep, substeps } = this.state;

        if (currentStep === 5) {
            const lastSubstep = substeps[substeps.length - 1];
            this.setState({
                currentSubstep: lastSubstep,
            });

            currentStep = 4;
            if (substeps.length === 0){
                if (this.state.expenseChecked){
                    currentStep = 3;
                }else{
                    currentStep = 2;
                }
            }
        } else if (currentStep === 4) {
            const isFirstSubstep = currentSubstep === substeps[0];

            if (!isFirstSubstep) {
                const currentSubstepIndex = substeps.indexOf(currentSubstep);
                const prevSubstepIndex = currentSubstepIndex - 1;
                const prevSubstep = substeps[prevSubstepIndex];

                this.setState({
                    currentSubstep: prevSubstep,
                });
            } else {
                // step 3 is optional expense step, skip to step 2 if "expenses not checked"
                if (!this.state.expenseChecked) {
                    currentStep = 2;
                } else {
                    currentStep = 3;
                }
            }
        } else {
            currentStep -= 1;
        }

        this.setState({ currentStep });
    };

    renderNavigationButtons = () => {
        const selectedSubfields = Object.entries(this.state.subfields)
            .filter(([key, val]) => (val[0]))
            .map(([key, val]) => (key));

        const progressBarValue = (this.state.currentStep + this.state.substeps.indexOf(this.state.currentSubstep)-3)/(selectedSubfields.length+1);

        const progressBar = this.state.currentStep > 2 && (
            //needs to be formatted with css
            <div id="progressBar">
                <div id="formProgressBar">{this.state.currentStep + this.state.substeps.indexOf(this.state.currentSubstep)-3}/{selectedSubfields.length+1}</div>
            </div>
        );

        const prevButton = this.state.currentStep > 1 && (
            <button
                className="btn btn-secondary"
                type="button"
                onClick={this._prev}
            >
                Back
            </button>
        );
        const nextButton = this.state.currentStep < 5 && (
            <button
                className="btn btn-primary"
                type="button"
                onClick={this._next}
            >
                Next
            </button>
        );

        const submitButton = this.state.currentStep === 5 && (
            <SubmitButton
                subfields={this.state.subfields}
                upfrontCost={this.state.upfrontCost}
                annualCost={this.state.annualCost}
                currentStep={this.state.currentStep}
            />
        );

        return (
            <div className="footer">
                {progressBar}
                {prevButton}
                {nextButton}
                {submitButton}
            </div>
        );
    }

    handleChangeCheckbox = event => {
        const { name } = event.target;
        const updatedSubs = { ...this.state.subfields };
        updatedSubs[name][0] = !this.state.subfields[name][0];
        const selectedSubfields = Object.entries(this.state.subfields)
            .filter(([key, val]) => (val[0]))
            .map(([key, val]) => (key));

        this.setState({
            subfields: updatedSubs,
            substeps: selectedSubfields,
            currentSubstep: selectedSubfields[0],
        });
    };

    handleChangeCheckboxExpense = event => {
        const {name} = event.target;
        var curChecked = this.state.expenseChecked;
        this.setState({
            expenseChecked: !curChecked
        });
    };

    handleUpdateCompanyData(name, value) {
        this.companyData[name] = parseInt(value);
    };

    submitExpenseForm = () => {
        console.log(this);
        if (this.state.expenseName === "" ||
            this.state.expenseValue === "" ||
            this.state.expenseChange === "") {
            alert("All fields must be filled to submit");
            console.log (this.state.expenseName);
            console.log (this.state.expenseValue);
            console.log (this.state.expenseChange);
        }
        else {
            this.state.expenseValues.push([this.state.expenseName, this.state.expenseValue, this.state.expenseChange, this.state.expenseUnit]);
            this.state.expenseName = "";
            this.state.expenseValue = "";
            this.state.expenseChange = "";
        }
        console.log(this.state.expenseValues);
        this.forceUpdate();
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit} style={this.props.style}>
                <Step1
                    currentStep={this.state.currentStep}
                    handleChange={this.handleChange}
                    ideaName={this.state.ideaName}
                />
                <Step2
                    currentStep={this.state.currentStep}
                    handleChange={this.handleChange}
                    handleLoad={this.handleLoad}
                    handleChangeCheckbox={this.handleChangeCheckbox}
                    handleChangeCheckboxExpense={this.handleChangeCheckboxExpense}
                    parentState={this.state}
                    category={this.state.category}
                />
                <ExpenseStep
                    currentStep={this.state.currentStep}
                    handleChange={this.handleChange}
                    handleLoad={this.handleLoad}
                    handleSubmitExpense={this.submitExpenseForm}
                    parentState={this.state}
                    category={this.state.category}
                />
                {this.state.currentStep === 4 && (
                    <Step4
                        updateSubfieldValue={this.updateSubfieldValue}
                        subfields={this.state.subfields}
                        substeps={this.state.substeps}
                        currentSubstep={this.state.currentSubstep}
                        setSubsteps={this.setSubsteps}
                        setCurrentSubstep={this.setCurrentSubstep}
                        password={this.state.password}
                        percentChange={this.state.percentChange}
                        companyData={this.props.companyData}
                        assumptions={this.state.assumptions}
                        handleUpdateCompanyData={this.handleUpdateCompanyData}
                    />
                )}
                <Step5
                    currentStep={this.state.currentStep}
                    handleChange={this.handleChange}
                    password={this.state.password}
                    upfrontCost={this.state.upfrontCost}
                    annualCost={this.state.annualCost}
                />
                {this.renderNavigationButtons()}
            </form>
        );
    }
}

function Step1(props) {
    if (props.currentStep !== 1) {
        return null;
    }
    return (
        <div className="form-group">
            <div className="text-1 mb24">
                Please enter a name for your idea:
            </div>
            <input
                className="form-control"
                id="ideaName"
                name="ideaName"
                placeholder="Enter Name"
                value={props.ideaName}
                onChange={props.handleChange}
                onKeyPress={(e) => {
                    e.key === "Enter" && e.preventDefault();
                }}
            />
        </div>
    );
}

const Step2 = (props) => {
    if (props.currentStep !== 2) {
        return null;
    }

    const subcategoryMap = {
        "Culture / Office": ["recruitingCosts","turnoverRate","rent"],
        "Sales": ["price","volume"],
        // "The Office": ["recruitingCosts","retention","rent"],`
        "Capital": ["debt","interestRate"],
        "Profitability": ["margin"],
        // "Talent": ["recruitingCosts","retention"],
        "Inventory": ["directCosts","indirectCosts","carryingCosts","productionCapacity","qualityControl"]
    };

    return(
        <div className="form-group">
            <div className="text-1 mb36">
                Please select the relevant impact areas for this idea (select all that apply):
            </div>
            <div>
                {Object.entries(subcategoryMap).map(([categoryName, subcategoriesArray]) => {
                    let checkedCategoriesCount = 0;
                    return (
                        <div key={categoryName} className="collapsible-container">
                            <Collapsible
                                key={categoryName}
                                transitionTime={200}
                                trigger={
                                    <div className="category-name">
                                        <ArrowDropDownIcon />
                                        {categoryName}
                                    </div>
                                }
                            >
                                {subcategoriesArray.map(subcategory => {
                                    if (props.parentState.subfields[subcategory][0]) {
                                        checkedCategoriesCount++;
                                    }
                                    return (
                                        <div key={subcategory}>
                                            <FormControlLabel
                                                label={addSpaceToCamelCase(subcategory)}
                                                control={
                                                    <Checkbox
                                                        checked={props.parentState.subfields[subcategory][0]}
                                                        name={subcategory}
                                                        color="primary"
                                                        onChange={props.handleChangeCheckbox}
                                                    />
                                                }
                                            />
                                        </div>
                                    );
                                })}
                            </Collapsible>
                            {!!checkedCategoriesCount && <div className="num-selected">{checkedCategoriesCount} Selected</div>}
                        </div>
                    );
                })}
                <div className="collapsible-container">
                    <Collapsible
                        trigger={<div className="category-name"><ArrowDropDownIcon /> Expenses </div>}
                        transitionTime={200}
                    >
                        <FormControlLabel
                            label="Custom Expenses"
                            control={
                                <Checkbox
                                    checked={props.parentState.expenseChecked}
                                    name={"Custom Expenses"}
                                    color="primary"
                                    onChange={props.handleChangeCheckboxExpense}
                                />
                            }
                        />
                    </Collapsible>
                    {props.parentState.expenseChecked && <div className="num-selected">1 Selected</div>}
                </div>
            </div>
        </div>
    );
}

function ExpenseStep(props) {
    if (props.currentStep !== 3) {
        return null;
    }

    const selectOptions = ["%", "$/#"];
    const formattedOptions = selectOptions.map((o) => <option key={o}>{o}</option>);

    return (
        <>
            <div className="form-group">
                <div className="text-1 mb20">Expense Name</div>
                <input
                    className="form-control mb20"
                    id="expenseName"
                    name="expenseName"
                    placeholder="Enter Name"
                    value={props.parentState.expenseName}
                    onChange={props.handleChange}
                    onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                />
                <div className="text-1 mb20">Original Value of the Expense</div>
                <input
                    className="form-control mb20"
                    id="expenseValue"
                    name="expenseValue"
                    placeholder="Enter Name"
                    value={props.parentState.expenseValue}
                    onChange={props.handleChange}
                    onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                />

                <div className="text-1 mb20">Expense will change by how much? (negative for decrease)</div>
                <div className="form-control-container mb20">
                    <input
                        className="form-control"
                        id="expenseChange"
                        name="expenseChange"
                        value={props.parentState.expenseChange}
                        onChange={props.handleChange}
                        onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                    />
                    <select
                        id="expenseUnit"
                        name="expenseUnit"
                        value={props.parentState.expenseUnit}
                        onChange={props.handleChange}
                    >
                        {formattedOptions}
                    </select>
                </div>
             </div>

            <div>
                <button
                    className="btn btn-primary"
                    type="button"
                    onClick={props.handleSubmitExpense}
                    style={{ padding: "0 20px"}}
                >
                    Submit Expense/Add New Expense
                </button>
            </div>
        </>
    );
}


const Step4 = (props) => {
    const { currentSubstep, subfields, updateSubfieldValue, companyData, assumptions, handleUpdateCompanyData } = props;
    const selectOptions = ["%", "$/#"];
    const formattedOptions = selectOptions.map((o) => <option key={o}>{o}</option>);
    if (!currentSubstep){
        return null;
    }
    const currentSubfieldValue = currentSubstep ? subfields[currentSubstep][1] : '';

    const currentSubfieldInput = (
        <div key={`field-${currentSubstep}`}>
            <div className="text-1 mb20">
                How much do you estimate {addSpaceToCamelCase(currentSubstep)} changing by?
                <HelperTooltip
                    tooltipText={`Enter a positive or negative change to the value of ${currentSubstep}.`}
                />
            </div>
            <div className="form-control-container">
                <input
                    className="form-control"
                    name={currentSubstep}
                    value={currentSubfieldValue}
                    placeholder="Enter positive or negative amount"
                    onChange={event => { updateSubfieldValue(currentSubstep, event.target.value); }}
                    onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                />
                <select
                    className="select"
                    name={currentSubstep}
                    onChange={(event) => {
                        subfields[event.target.name] = [subfields[event.target.name][0], subfields[event.target.name][1], event.target.value];
                    }}
                >
                    {formattedOptions}
                </select>
                <ArrowDropDownIcon />
            </div>
            <div className="helper-text">
                Original value of {addSpaceToCamelCase(currentSubstep)} is {companyData[currentSubstep]}. The new value will be { (subfields[currentSubstep][2] === "%") ? companyData[currentSubstep]*(1+(subfields[currentSubstep][1]/100.0)) : Number(companyData[currentSubstep])+ Number(subfields[currentSubstep][1])}.
            </div>
        </div>
    );

    // assumptions which are associated with current subfield
    const currentAssumptions = Object.entries(subfields)
        .filter(([key, val]) => (val[0] && assumptions[key].length && key === currentSubstep))
        .map(([key, val]) => (
            <div key={`assumption-${key}`}>
                {Object.entries(assumptions[key]).map(([assumption, curAssumption]) => (
                    <div key={assumption} className="mb20">
                        <div className="text-1 mb20 capitalize">{addSpaceToCamelCase(curAssumption)} value:</div>
                        <div className="editable-label-wrapper">
                            <EditableLabel
                                labelClassName="foobar-label"
                                inputClassName="foobar-input"
                                text={String(companyData[curAssumption])}
                                onFocusOut={event => { handleUpdateCompanyData(curAssumption, event); }}
                            />
                        </div>
                    </div>
                ))}
            </div>
        ));


    return (
        <>
            {currentSubfieldInput}
            {!!currentAssumptions.length && (
                <Collapsible
                    key={"AssumptionCollapsible"}
                    transitionTime={200}
                    trigger={
                        <div className="category-name">
                            <ArrowDropDownIcon />
                            {"Assumptions"}
                        </div>
                    }
                >
                    <div className="assumptions-container">
                        <div className="text-2 G3 mb20">
                            Assumptions
                            <HelperTooltip
                                tooltipText={`The following assumed values relate to ${currentSubstep}, and will be used in value calculations.`}
                            />
                        </div>
                        {currentAssumptions}
                    </div>
                </Collapsible>
            )}
        </>
    );
}

function Step5(props) {
    if (props.currentStep !== 5) {
        return null;
    }
    return(
        <>
            <div className="form-group">
                <div className="text-1 mb20">Enter one-time upfront costs:</div>
                <div className="form-control-container mb40">
                    <input
                        className="form-control"
                        id="upfrontcost"
                        name="upfrontCost"
                        placeholder="Enter amount"
                        value={props.upfrontCost}
                        onChange={props.handleChange}
                        onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                    />
                    <select
                        className="select"
                        onChange={(event) => {}}
                    >
                        <option>$</option>
                    </select>
                </div>
                <div className="text-1 mb20">Enter annual costs:</div>
                <div className="form-control-container">
                    <input
                        className="form-control"
                        id="annualcost"
                        name="annualCost"
                        placeholder="Enter amount"
                        value={props.annualCost}
                        onChange={props.handleChange}
                        onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                    />
                    <select
                        className="select"
                        onChange={(event) => { }}
                    >
                        <option>$</option>
                    </select>
                </div>
            </div>
        </>
    );
}

function SubmitButton(props){
    let disabled = false;

    for (var key in props.subfields){
        if (props.subfields[key]) {
            if (props.subfields[key][0]) {
                if (props.subfields[key][1] === 0) {
                    disabled = true;
                }
            }
        }
    }

    if (props.upfrontCost === "" || props.annualCost === ""){
        disabled = true;
    }

    return(
        <button
            className="btn btn-primary btn-green"
            disabled={disabled}
        >
            Calculate
        </button>
    );
}

export default IdeaForm;
