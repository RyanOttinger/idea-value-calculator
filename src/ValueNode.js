import React from 'react';
// import ReactDOM from 'react-dom';
import Collapsible from 'react-collapsible';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

const currencyFormat = (number) => {
    const options = {
        style: 'currency',
        currency: 'USD',
    };

    return new Intl.NumberFormat('en-US', options).format(number);
};

class ValueNode extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            newCompanyData: {},
        salesB: false,
        turnoverRateB: false,
        inventoryB: false,
        debtB: false,
        recruitingB: false,
        rentB: false,}
    }

    render() {
        var yearlyValue = Calculations(this.props, this.state);

        var upfrontCost = parseFloat(this.props.upfrontCost);
        var annualCost = parseFloat(this.props.annualCost)

        var firstYear = (yearlyValue - upfrontCost - annualCost);
        var fiveYear = (yearlyValue * 5- upfrontCost - annualCost * 5);
        let firstYearROI;
        let fiveYearROI;

        if (upfrontCost + annualCost === 0) {
            firstYearROI = "N/A (No Investment)";
            fiveYearROI = "N/A (No Investment)";
        } else {
            firstYearROI = Math.round((firstYear / (upfrontCost + annualCost))*100);
            fiveYearROI = Math.round((fiveYear / (upfrontCost + annualCost * 5))*100);
        }

        const getStyleFromValue = (value) => {
            let fontColor = '#2d3941'; // $G1
            if (value > 0) {
                fontColor = '#27b87e'; // $green
            } else if (value < 0) {
                fontColor = '#b82845'; // $red
            }

            return { color: fontColor };
        }

        return (
            <React.Fragment>
                <div className="value-node">
                    <h4>{this.props.ideaName}</h4>
                    <div className="value-node-tiles">
                        <div className="value-node-tile">
                            <div className="text-1 mb20">1 Year</div>
                            <div
                                className="text-3 mb16"
                                style={getStyleFromValue(firstYear)}
                            >
                                {currencyFormat(firstYear)}
                            </div>
                            <div className="text-2 G3 mb36">Return</div>
                            <div
                                className="text-3 mb16"
                                style={getStyleFromValue(firstYearROI)}
                            >
                                {firstYearROI}%
                            </div>
                            <div className="text-2 G3">ROI</div>
                        </div>
                        <div className="value-node-tile">
                            <div className="text-1 mb20">5 Year</div>
                            <div
                                className="text-3 mb16"
                                style={getStyleFromValue(fiveYear)}
                            >
                                {currencyFormat(fiveYear)}
                            </div>
                            <div className="text-2 G3 mb36">Return</div>
                            <div
                                className="text-3 mb16"
                                style={getStyleFromValue(fiveYearROI)}
                            >
                                {fiveYearROI}%
                            </div>
                            <div className="text-2 G3">ROI</div>
                        </div>
                    </div>
                    <Formulas parentProps={this.props} state={this.state} annualReturn={yearlyValue} />
                </div>
            </React.Fragment>
        );
    }
}

function Calculations(props, state)
{
    var localNewCompanyData = {};

    var totalAnnualReturn = 0;

    // console.log(props.subfields);
    for (var key in props.subfields){
        if (props.subfields[key]) {
            if (props.subfields[key][0]) {
                if (props.subfields[key][2] === "%"){
                    localNewCompanyData[key] = (1.0 + props.subfields[key][1]/100)*props.companyData[key];
                } else {
                    localNewCompanyData[key] =  parseInt(props.companyData[key],10) + parseInt(props.subfields[key][1], 10);
                }
                // console.log(props.companyData[key]);
                if (key === "price" || key === "volume" || key === "margin"){
                    state.salesB = true;
                } else if (key === "turnoverRate"){
                    state.turnoverRateB = true;
                } else if (key === "directCosts" || key === "indirectCosts" || key === "carryingCosts" || key === "productionCapacity" || key === "qualityControl"){
                    state.inventoryB = true;
                } else if (key === "debt" || key === "interestRate"){
                    state.debtB = true;
                } else if (key === "recruitingCosts"){
                    state.recruitingB = true;
                } else if (key === "rent"){
                    state.rentB = true;
                }
            }
        }
    }

    for (var key in props.companyData) {
        if (!(localNewCompanyData[key])){
            localNewCompanyData[key] = props.companyData[key];
        }
    }
    state.newCompanyData = localNewCompanyData;
    var newCompanyData = localNewCompanyData;

    if (state.salesB){
        // console.log("New Price: " + newCompanyData.price + " New Volume: " + newCompanyData.volume + " New Margin: " + newCompanyData.margin + "%");
        totalAnnualReturn += ((newCompanyData.price*newCompanyData.volume*newCompanyData.margin/100) - (props.companyData.totalAnnualSales*props.companyData.margin/100));
    }


    if (state.turnoverRateB){
        //console.log(newCompanyData.turnoverRate);
        //console.log((newCompanyData.headcount * newCompanyData.onboardingCost * newCompanyData.turnoverRate / 100.0));
        //console.log((props.companyData.headcount * props.companyData.onboardingCost * props.companyData.turnoverRate / 100.0));
        totalAnnualReturn -= ((newCompanyData.headcount * newCompanyData.onboardingCost * newCompanyData.turnoverRate / 100.0) - (props.companyData.headcount * props.companyData.onboardingCost * props.companyData.turnoverRate / 100.0));
    }

    if (state.inventoryB){
        // console.log("inventory: " + totalAnnualReturn);
        totalAnnualReturn +=
            (newCompanyData.carryingCosts + newCompanyData.directCosts + newCompanyData.indirectCosts + newCompanyData.materialCosts - ((1-(newCompanyData.qualityControl/100))*(newCompanyData.materialCosts + newCompanyData.directCosts + newCompanyData.indirectCosts))) -
            (props.companyData.carryingCosts + props.companyData.directCosts + props.companyData.indirectCosts + props.companyData.materialCosts - ((1-(props.companyData.qualityControl/100))*(props.companyData.materialCosts + props.companyData.directCosts + props.companyData.indirectCosts)));
        // console.log(totalAnnualReturn);
    }

    if (state.debtB){
        // console.log("debt: " + totalAnnualReturn);
        totalAnnualReturn -= (newCompanyData.interestRate * newCompanyData.debt) - (props.companyData.interestRate * props.companyData.debt);
        // console.log(totalAnnualReturn);
    }

    if (state.recruitingB){
        // console.log("recruitingCosts: " + totalAnnualReturn);
        totalAnnualReturn -= newCompanyData.recruitingCosts - props.companyData.recruitingCosts;
        // console.log(totalAnnualReturn);
    }

    if (state.rentB){
        // console.log("rent: " + totalAnnualReturn);
        totalAnnualReturn -= newCompanyData.rent - props.companyData.rent;
        // console.log(totalAnnualReturn);
    }

    for (var index in props.expenseFields){
        if (props.expenseFields[index][3] === '%') {
            totalAnnualReturn -= props.expenseFields[index][1] * props.expenseFields[index][2]/100;
        } else{
            totalAnnualReturn -= props.expenseFields[index][1];
        }
    }

    return totalAnnualReturn;
}

function FindBreakevenPoint(key, companyData, annualReturn){
    switch(key){
        case 'price':
            return (companyData.price*companyData.volume*companyData.margin/100 - annualReturn)/(companyData.volume*companyData.margin/100);
        case 'volume':
            return (companyData.price*companyData.volume*companyData.margin/100 - annualReturn)/(companyData.price*companyData.margin/100);
        case 'margin':
            return 100*(companyData.price*companyData.volume*companyData.margin/100 - annualReturn)/(companyData.volume*companyData.price);
        case 'turnoverRate':
            return 100*(annualReturn + (companyData.headcount * companyData.onboardingCost * companyData.turnoverRate / 100.0))/(companyData.headcount * companyData.onboardingCost);
        case 'debt':
            return (annualReturn + (companyData.interestRate * companyData.debt))/companyData.interestRate;
        case 'interestRate': //this case is broken.
            return (annualReturn + (companyData.interestRate * companyData.debt))/companyData.debt;
        case 'directCosts':
            return (((companyData.carryingCosts + companyData.directCosts + companyData.indirectCosts + companyData.materialCosts - ((1-(companyData.qualityControl/100))*(companyData.materialCosts + companyData.directCosts + companyData.indirectCosts)))- annualReturn) - companyData.carryingCosts - companyData.indirectCosts - companyData.materialCosts + ((1-(companyData.qualityControl/100))*(companyData.materialCosts + companyData.indirectCosts)))/(1 - ((1-(companyData.qualityControl/100))));
        case 'indirectCosts':
            return (((companyData.carryingCosts + companyData.directCosts + companyData.indirectCosts + companyData.materialCosts - ((1-(companyData.qualityControl/100))*(companyData.materialCosts + companyData.directCosts + companyData.indirectCosts)))- annualReturn) - companyData.carryingCosts - companyData.directCosts - companyData.materialCosts + ((1-(companyData.qualityControl/100))*(companyData.materialCosts + companyData.directCosts)))/(1 - ((1-(companyData.qualityControl/100))));
        case 'carryingCosts':
            return (((companyData.carryingCosts + companyData.directCosts + companyData.indirectCosts + companyData.materialCosts - ((1-(companyData.qualityControl/100))*(companyData.materialCosts + companyData.directCosts + companyData.indirectCosts)))- annualReturn) - companyData.directCosts - companyData.indirectCosts - companyData.materialCosts + ((1-(companyData.qualityControl/100))*(companyData.materialCosts + companyData.indirectCosts + companyData.directCosts)));
        case 'productionCapacity'://not implemented
            return;
        case 'qualityControl':
            return -100*((((companyData.carryingCosts + companyData.directCosts + companyData.indirectCosts + companyData.materialCosts - ((1-(companyData.qualityControl/100))*(companyData.materialCosts + companyData.directCosts + companyData.indirectCosts)))- annualReturn) - companyData.carryingCosts - companyData.directCosts - companyData.indirectCosts - companyData.materialCosts)/(-1*(companyData.materialCosts + companyData.directCosts + companyData.indirectCosts))-1);
        default:
            return (annualReturn + companyData[key]);
    }
}
function Formulas(props) {
    const formulas = {
        salesB: 'Annual Return += (New Price * New Volume * New Margin) - Old Profit',
        turnoverRateB: 'Annual Return += ((New Headcount * New Onboarding Cost * New Turnover Rate) - (Old Headcount * Old Onboarding Cost * Old Turnover Rate))',
        inventoryB: 'Annual Return += (New Carrying Costs + New Direct Costs + New Indirect Costs + New Material Costs - ((1-(New Quality Control/100))*(New Material Costs + New Direct Costs + New Indirect Costs))) - (Old Carrying Costs + Old Direct Costs + Old Indirect Costs + Old Material Costs - ((1-(Old Quality Control/100))*(Old Material Costs + Old Direct Costs + Old Indirect Costs)))',
        debtB: 'Annual Return -= (New Interest Rate * New Debt) - (Old Interest Rate * Old Debt)',
        recruitingB: 'Annual Return -= New Recruiting Costs - Old Recruiting Costs',
        rentB: 'Annual Return -= New Rent - Old Rent',
    };

    const numericFormulas = {
        salesB: 'Annual Return += (' + props.state.newCompanyData['price'] + ' * ' + props.state.newCompanyData['volume'] + ' * ' + (props.state.newCompanyData['margin'] / 100) + ') - ' + (props.parentProps.companyData['totalAnnualSales'] * props.parentProps.companyData['margin'] / 100),
        turnoverRateB: 'Annual Return += ((' + props.state.newCompanyData['headcount'] + ' * ' + props.state.newCompanyData['onboardingCost'] + ' * ' + (props.state.newCompanyData['turnoverRate'] / 100) + ') - (' + props.parentProps.companyData['headcount'] + ' * ' + props.parentProps.companyData['onboardingCost'] + ' * ' + (props.parentProps.companyData['turnoverRate'] / 100) + '))',
        inventoryB: 'Annual Return += (New Carrying Costs + New Direct Costs + New Indirect Costs + New Material Costs - ((1-(New Quality Control/100))*(New Material Costs + New Direct Costs + New Indirect Costs))) - (Old Carrying Costs + Old Direct Costs + Old Indirect Costs + Old Material Costs - ((1-(Old Quality Control/100))*(Old Material Costs + Old Direct Costs + Old Indirect Costs)))',
        debtB: 'Annual Return -= (' + props.state.newCompanyData['interestRate'] + ' * ' + props.state.newCompanyData['debt'] + ') - (' + props.parentProps.companyData['interestRate'] + ' * ' + props.parentProps.companyData['debt'] + ')',
        recruitingB: 'Annual Return += ' + props.state.newCompanyData['recruitingCosts'] + ' - ' + props.parentProps.companyData['recruitingCosts'],
        rentB: 'Annual Return -= ' + props.state.newCompanyData['rent'] + ' - ' + props.parentProps.companyData['rent'],
    };

    const numericResults = {
        salesB: 'Annual Return += ' + (Math.round(((props.state.newCompanyData['price'] *  props.state.newCompanyData['volume'] * (props.state.newCompanyData['margin']/100)) - (props.parentProps.companyData['totalAnnualSales'] * props.parentProps.companyData['margin']/100))*100)/100),
        turnoverRateB: 'Annual Return += '+ (Math.round(((props.state.newCompanyData['headcount'] * props.state.newCompanyData['onboardingCost'] * (props.state.newCompanyData['turnoverRate']/100)) - (props.parentProps.companyData['headcount'] * props.parentProps.companyData['onboardingCost'] * (props.parentProps.companyData['turnoverRate']/100) ))*100)/100),
        inventoryB: 'Annual Return += (New Carrying Costs + New Direct Costs + New Indirect Costs + New Material Costs - ((1-(New Quality Control/100))*(New Material Costs + New Direct Costs + New Indirect Costs))) - (Old Carrying Costs + Old Direct Costs + Old Indirect Costs + Old Material Costs - ((1-(Old Quality Control/100))*(Old Material Costs + Old Direct Costs + Old Indirect Costs)))',
        debtB: 'Annual Return -= ' +  Math.round(((props.state.newCompanyData['interestRate'] * props.state.newCompanyData['debt']) - ( props.parentProps.companyData['interestRate'] * props.parentProps.companyData['debt']))*100)/100,
        recruitingB: 'Annual Return += ' + Math.round((props.state.newCompanyData['recruitingCosts'] - props.parentProps.companyData['recruitingCosts'])*100)/100,
        rentB: 'Annual Return -= ' + Math.round((props.state.newCompanyData['rent'] - props.parentProps.companyData['rent'])*100)/100,
    };

    const getNumStr = (str) => {
        const split = str.split('=');
        const numStr = split[split.length - 1];
        return numStr;
    };

    const sumNumStrs = (arr) => {
        let sum = 0;
        for (const numStr of arr) {
            sum += Number(numStr);
        }
        return sum;
    };


    const numStrs = []; // map over these to show the summary line of all subfields which contribute to annual return
    const formulaGroup = {}; // we will map over these to display 3 formulas per group, and one group for each selected subfield
    Object.keys(formulas)
        .filter(key => props.state[key])
        .forEach(key => {
            // remove the B from key name for display purposes. the B stands for boolean
            const shortenedKey = key.substring(0, key.length - 1);
            const capitalizedKey = `${shortenedKey[0].toUpperCase()}${shortenedKey.substring(1, shortenedKey.length)}`;
            formulaGroup[capitalizedKey] = [
                formulas[key],
                numericFormulas[key],
                numericResults[key],
            ];

            if (key !== 'inventoryB') {
                // numericResults were never entered for "inventoryB"
                numStrs.push(getNumStr(numericResults[key]));
            }
        });


    const variableChanges = {};
    for (var key in props.parentProps.companyData) {
        if (props.state.newCompanyData[key] != props.parentProps.companyData[key]){
            variableChanges[key] = "" + key +": " + (Math.round(props.parentProps.companyData[key]*100)/100) + " to " + (Math.round(props.state.newCompanyData[key]*100)/100) + " Breakeven Point: " + Math.round(100*FindBreakevenPoint(key, props.state.newCompanyData, props.annualReturn))/100;
        }
    }

    return (
        <div className="collapsible-container">
            <Collapsible
                transitionTime={200}
                trigger={
                    <div className="category-name">
                        <ArrowDropDownIcon />
                        Calculations
                    </div>
                }
            >
                <div className="formula-group" key={"VarChanges"}>
                    <div className="formula-group-title">Variable Changes:</div>
                    {Object.keys(variableChanges).map((key) => (<div key={key} className="formula">{variableChanges[key]}</div>))}
                </div>

                {Object.entries(formulaGroup).map(([key, group]) => (
                    <div className="formula-group" key={key}>
                        <div className="formula-group-title">Formulas relating to {key}:</div>
                        {group.map(formula => (<div key={formula} className="formula">{formula}</div>))}
                    </div>
                ))}
                {numStrs.length > 1 && (
                    <div className="formula-group">
                        {`Annual Return = ${numStrs.join(' + ')} = ${sumNumStrs(numStrs)}`}
                    </div>
                )}
            </Collapsible>
        </div>
    );
}

export default ValueNode;
