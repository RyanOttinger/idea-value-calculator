import React from 'react';
import './styles/App.css';

import IdeaForm from './IdeaForm.js';
import ValueNode from './ValueNode.js';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            companyData: {
                totalAnnualSales:1999950,
                price:150,
                volume: 13333,
                totalAssets:1000000,
                equityRate:15,
                margin:17,

                headcount:13.33,
                salary:47000,
                turnoverRate:10,
                onboardingCost:4129,
                recruitingCosts:9400,
                burden:17500,

                debt:0,
                interestRate:8,

                otherIncome:50000,

                taxAverage:30,
                taxMarginal:39,

                materialCosts:422500,
                directCosts:650000,
                indirectCosts:200000,
                carryingCosts:212500,
                productionCapacity:13333.33,
                qualityControl:85,

                rent:72000,
                utilities:10000,
                supplies:10000,
                otherCosts:0,

                brandValue:75000,
                capitalAssets:50000,
                intangibleAssets:10000,
                otherAssets:11300,

                otherLiabilities:17000
            },
            selectedOption: null,
            showNewNodeForm: true,
            valueNodes:[],
        }
    }

    appendValueNode = (name, subs, expenses, upfront, annual) => {
        const currentNodes = [
            <ValueNode
                ideaName={name}
                subfields={subs}
                expenseFields={expenses}
                upfrontCost={upfront}
                annualCost={annual}
                companyData={this.state.companyData}
            />
        ].concat([...this.state.valueNodes]);

        this.setState({
            valueNodes: currentNodes,
            showNewNodeForm: false,
        });
    }

    render() {
        return(
            <div className="app" >
                <div className="idea-valuation">
                    <div className="header">
                        <h2>Idea Valuation</h2>
                        {(!!this.state.valueNodes.length) && (
                            <div
                                className="text-link"
                                onClick={() => {
                                    this.setState({
                                        showNewNodeForm: !this.state.showNewNodeForm,
                                    });
                                }}
                            >
                                {this.state.showNewNodeForm ? 'Saved Ideas' : 'New Idea'}
                            </div>
                        )}
                    </div>
                    <div className={`main-content ${this.state.showNewNodeForm ? 'has-footer' : ''}`}>
                        <IdeaForm
                            key={this.state.valueNodes.length}
                            style={{ display: this.state.showNewNodeForm ? 'block' : 'none' }}
                            submitCallback={this.appendValueNode}
                            companyData={this.state.companyData}
                        />
                        {!this.state.showNewNodeForm && this.state.valueNodes}
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
