import React from 'react';

const HelperTooltip = (props) => (
    <span className="helper-tooltip">
      <span className="helper-tooltip-trigger">ⓘ</span>
      <span className="helper-tooltip-contents">{props.tooltipText}</span>
    </span>
);

HelperTooltip.defaultProps = {
  tooltipText: '',
};

export default HelperTooltip;
